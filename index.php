<!DOCTYPE html><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <title>Timeline</title>
    <link href="./css/main.css" rel="stylesheet" />
  </head>
  <body>
    <header class="header">
      <a class="header__brand" href="#">
        <img src="assets/images/logo_mx_1.jpg" alt="Multiplica">
      </a>
    </header>
    <main>
    </body>
      <div class="intro">
        <div class="intro__wrapper">
          <h1 class="mb-30">Bienvenido</h1>
          <p class="mb-30">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa nisi dolorem possimus expedita
            praesentium blanditiis, voluptate fuga saepe. Modi quam mollitia rem illum iure atque repellat fugiat eaque
            quidem blanditiis.</p><button class="intro__button">Ver historia</button>
        </div>
      </div>
      <ul class="timeline">
          <li class="timeline__item timeline__item--current">
            <div class="timeline__year"><h2>2020</h2></div>
            <ul class="timeline__milestones">
              <li class="milestone milestone--current">
                <div class="milestone__dot"></div>
                <div class="milestone__info">
                  <div class="d-flex justify-content-between align-items-center mb-10">
                    <span class="milestone__month">Febrero</span>
                    <button class="milestone__erase"><img src="assets/images/delete.png" alt="Borrar"></button>
                  </div>
                  <h3 class="milestone__title">Dato importante</h3>
                  <p class="milestone__text">Agregado por Jhon Doe</p>
                </div>
              </li>
              <li class="milestone milestone--current">
                <div class="milestone__dot"></div>
                <div class="milestone__info">
                  <div class="d-flex justify-content-between align-items-center mb-10">
                    <span class="milestone__month">Febrero</span>
                    <button class="milestone__erase"><img src="assets/images/delete.png" alt="Borrar"></button>
                  </div>
                  <h3 class="milestone__title">Dato importante</h3>
                  <p class="milestone__text">Agregado por Jhon Doe</p>
                </div>
              </li>
              <li class="milestone milestone--current">
                <div class="milestone__dot"></div>
                <div class="milestone__info">
                  <div class="d-flex justify-content-between align-items-center mb-10">
                    <span class="milestone__month">Febrero</span>
                    <button class="milestone__erase"><img src="assets/images/delete.png" alt="Borrar"></button>
                  </div>
                  <h3 class="milestone__title">Dato importante</h3>
                  <p class="milestone__text">Agregado por Jhon Doe</p>
                </div>
              </li>
              <li class="milestone milestone--current">
                <div class="milestone__dot"></div>
                <div class="milestone__info">
                  <div class="d-flex justify-content-between align-items-center mb-10">
                    <span class="milestone__month">Febrero</span>
                    <button class="milestone__erase"><img src="assets/images/delete.png" alt="Borrar"></button>
                  </div>
                  <h3 class="milestone__title">Dato importante</h3>
                  <p class="milestone__text">Agregado por Jhon Doe</p>
                </div>
              </li>
            </ul>
          </li>
      </ul>
      <div class="add-milestone">
        <button class="add-milestone__button">Agregar hito</button>
        <div class="add-milestone__form">
          <div class="p-30">
            <div class="d-flex justify-content-between align-items-center mb-10 mb-25">
              <h4 class="mb-0 font-size-24">Agregar hito</h4>
              <button class="add-milestone__close">X</button>
            </div>
            <form id="addMilestoneForm" method="post">
              <div class="row">
                <div class="col-sm-6 mb-20">
                  <label class="font-weight-bold">Año</label>
                  <select name="year"
                    ><option value="2001">2001</option
                    ><option value="2002">2002</option
                    ><option value="2003">2003</option
                    ><option value="2004">2004</option
                    ><option value="2005">2005</option
                    ><option value="2006">2006</option
                    ><option value="2007">2007</option
                    ><option value="2008">2008</option
                    ><option value="2009">2009</option
                    ><option value="2010">2010</option
                    ><option value="2011">2011</option
                    ><option value="2012">2012</option
                    ><option value="2013">2013</option
                    ><option value="2014">2014</option
                    ><option value="2015">2015</option
                    ><option value="2016">2016</option
                    ><option value="2017">2017</option
                    ><option value="2018">2018</option
                    ><option value="2019">2019</option
                    ><option value="2020">2020</option></select
                  >
                </div>
                <div class="col-sm-6 mb-20">
                  <label class="font-weight-bold">Mes</label>
                  <select name="month"
                    ><option value="0">No me acuerdo</option
                    ><option value="1">Enero</option
                    ><option value="2">Febrero</option
                    ><option value="3">Marzo</option
                    ><option value="4">Abril</option
                    ><option value="5">Mayo</option
                    ><option value="6">Junio</option
                    ><option value="7">Julio</option
                    ><option value="8">Agosto</option
                    ><option value="9">Septiembre</option
                    ><option value="10">Octubre</option
                    ><option value="11">Noviembre</option
                    ><option value="12">Diciembre</option></select
                  >
                </div>
                <div class="col-12 mb-20">
                  <label class="font-weight-bold">Mensaje</label>
                  <input name="title" />
                </div>
                <div class="col-12 text-center">
                  <button class="add-milestone__button">Agregar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </main>
    <script src="main.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="scripts.js"></script>
  </body>
</html>

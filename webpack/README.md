# Fértil - maqueta web

To get started, install the dependencies:

```
# Using npm
npm install
```

After that, start up Webpack Development Server:

```
npm run serve
```

You can play around with `/index.html` to see the effects of your changes.

To build a production bundle run:

```
npm run build
```

After that you will have a ready to deploy bundle at `/dist`

// Polyfills
// Elemement.closet Pollyfill
if (!Element.prototype.matches) {
  Element.prototype.matches = Element.prototype.msMatchesSelector ||
                              Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
  Element.prototype.closest = function(s) {
    var el = this;

    do {
      if (el.matches(s)) return el;
      el = el.parentElement || el.parentNode;
    } while (el !== null && el.nodeType === 1);
    return null;
  };
}

// General Variables
const body = document.querySelectorAll('body')[0];

// Milestone open
const milestones = document.querySelectorAll('.milestone')
const milestoneDots = document.querySelectorAll('.milestone__dot')

const onClickMilestoneDot = e => {
  const milestone = e.currentTarget.parentNode

  e.preventDefault()

  if (milestone.classList.contains('milestone--active')) {
    milestone.classList.remove('milestone--active')
  } else {
    Array.prototype.forEach.call(milestones, milestone => milestone.classList.remove('milestone--active'))
    milestone.classList.add('milestone--active')
  }
}

// Milestone add open
const milestoneaddButton = document.querySelector('.add-milestone__button')
const milestonecloseButton = document.querySelector('.add-milestone__close')

const onClickMilestoneAdd = e => {
  const milestoneAddForm = e.currentTarget.closest('.add-milestone').querySelector('.add-milestone__form')

  milestoneAddForm.classList.add('add-milestone__form--active')
  e.currentTarget.classList.add('add-milestone__button--disable')
}

const onClickMilestoneClose = e => {
  const milestoneAddForm = e.currentTarget.closest('.add-milestone').querySelector('.add-milestone__form')
  const milestoneAddButton = e.currentTarget.closest('.add-milestone').querySelector('.add-milestone__button')

  milestoneAddForm.classList.remove('add-milestone__form--active')
  milestoneAddButton.classList.remove('add-milestone__button--disable')
}

// Change logo
const year2007 = document.querySelector('[data-year="2007"]')
const year2016 = document.querySelector('[data-year="2016"]')
const logo = document.querySelector('.header__brand__image')

const changeLogo = () => {
  if( year2007.offsetTop <= 10 ) {
    logo.setAttribute('src', 'assets/images/logo_mx_1.jpg');
  }
  if( year2007.offsetTop >= 10 ) {
    logo.setAttribute('src', 'assets/images/logo_mx_2.jpg');
  }
  if( year2016.offsetTop >= 10 ) {
    logo.setAttribute('src', 'assets/images/logo_mx_3.jpg');
  }
}

// Intro
const intro = document.querySelector('.intro')
const introButton = document.querySelector('.intro__button')
const hideIntro = () => {
  intro.classList.add('d-none')
}

// Init
const init = () => {

  // Milestone open init
  Array.prototype.forEach.call(milestoneDots, milestoneDot => {
    milestoneDot.addEventListener('click', onClickMilestoneDot, false);
  });

  // Milestone add init
  if ( milestoneaddButton ) {
    milestoneaddButton.addEventListener('click', onClickMilestoneAdd, false);
  }

  if ( milestonecloseButton ) {
    milestonecloseButton.addEventListener('click', onClickMilestoneClose, false);
  }

  window.addEventListener('scroll', changeLogo);

  if ( introButton ) {
    introButton.addEventListener('click', hideIntro, false);
  }
};

document.addEventListener('DOMContentLoaded', init);
